package com.app.myrewards.activity

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.app.myrewards.R
import com.app.myrewards.fragment.EnterCodeFragment
import com.app.myrewards.fragment.PointsFragment
import com.app.myrewards.fragment.RewardsFragment
import com.app.myrewards.fragment.ScannerFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_home.*
import permissions.dispatcher.*


@RuntimePermissions
class HomeActivity : AppCompatActivity() {

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_points -> {
                showPointsFragment()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_enter_code -> {
                showEnterCodeFragment()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_rewards -> {
                //TODO Rewards
                showRewardsFragment()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_scan -> {
                showCameraWithPermissionCheck()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_profile -> {
                //TODO Profile
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        showPointsFragment()
    }

    private fun showPointsFragment() {
        supportFragmentManager.beginTransaction().replace(
            getFragmentLayout(),
            PointsFragment()
        ).commitAllowingStateLoss()//Crash prevent
    }

    private fun showEnterCodeFragment() {
        supportFragmentManager.beginTransaction().replace(
            getFragmentLayout(),
            EnterCodeFragment()
        ).commitAllowingStateLoss()//Crash prevent
    }

    private fun showRewardsFragment() {
        supportFragmentManager.beginTransaction().replace(
            getFragmentLayout(),
            RewardsFragment()
        ).commitAllowingStateLoss()//Crash prevent
    }

    private fun showScanner() {
        supportFragmentManager.beginTransaction().replace(
            getFragmentLayout(),
            ScannerFragment()
        ).commitAllowingStateLoss()//Crash prevent
//
//        val transaction = getSupportFragmentManager().beginTransaction();
//        transaction.replace(getFragmentLayout(), ScannerFragment());
//        transaction.addToBackStack(getSupportFragmentManager().getBackStackEntryCount() == 0 ? KEY_FRAG_FIRST : null).commit();
    }

    private fun getFragmentLayout(): Int {
        return R.id.homeLayout
    }

    @NeedsPermission(Manifest.permission.CAMERA)
    fun showCamera() {
        showScanner()
    }

    @OnShowRationale(Manifest.permission.CAMERA)
    fun OnShowRationale(request: permissions.dispatcher.PermissionRequest) {
        AlertDialog.Builder(this)
            .setPositiveButton(R.string.button_allow) { _, _ ->
                request.proceed()
            }
            .setNegativeButton(R.string.button_deny) { _, _ ->
                request.cancel()
            }
            .setCancelable(false)
            .setMessage(R.string.permission_rationale)
            .show()
    }


    @OnPermissionDenied(Manifest.permission.CAMERA)
    fun onCameraDenied() {
//        Toast.makeText(this, R.string.permission_denied, Toast.LENGTH_SHORT).show()
        showSettingsSnackBar(resources.getString(R.string.permission_denied), false)
    }

    @OnNeverAskAgain(Manifest.permission.CAMERA)
    fun OnNeverAskAgain() {
        showSettingsSnackBar(resources.getString(R.string.permission_denied), false)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // NOTE: delegate the permission handling to generated function
        onRequestPermissionsResult(requestCode, grantResults)
    }

    private fun showSettingsSnackBar(msg: String, infite: Boolean) {
        val snackbar =
            Snackbar.make(
                findViewById(android.R.id.content),
                msg,
                if (infite) Snackbar.LENGTH_INDEFINITE else Snackbar.LENGTH_LONG
            )
        snackbar.setAction("", View.OnClickListener {
            startInstalledAppDetailsActivity(this@HomeActivity)
        })
        snackbar.show()
    }

    private fun startInstalledAppDetailsActivity(context: Activity?) {
        if (context == null) return
        val i = Intent()
        i.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        i.addCategory(Intent.CATEGORY_DEFAULT)
        i.data = Uri.parse("package:" + context.packageName)
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
        context.startActivity(i)
    }
}
