package com.app.myrewards.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.FragmentManager
import com.app.myrewards.R
import com.app.myrewards.fragment.BaseFragment
import com.app.myrewards.fragment.LoginFragment
import com.app.myrewards.views.TitleBar
import kotlinx.android.synthetic.main.activity_dock.*


class MainActivity : DockActivity() {

    private var mContext: MainActivity? = null
    private var loading: Boolean = false
    var titleBar: TitleBar? = null

    override fun onLoadingStarted() {
        if (mainFrameLayout != null) {
            mainFrameLayout.visibility = View.VISIBLE
            if (progressBar != null) {
                progressBar.visibility = View.VISIBLE
            }
            loading = true
        }
    }

    override fun onLoadingFinished() {
        mainFrameLayout.visibility = View.VISIBLE

        if (progressBar != null) {
            progressBar.visibility = View.INVISIBLE
        }
        loading = false
    }

    override fun onProgressUpdated(percentLoaded: Int) {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dock)

        titleBar = header_main
        mContext = this
        initFragment()
    }

    fun initFragment() {
        supportFragmentManager.addOnBackStackChangedListener(getListener())
        if (prefHelper.isLogin) {
            val intent = Intent(this@MainActivity, HomeActivity::class.java)
            finish()
            startActivity(intent)
        } else {
            replaceDockableFragment(LoginFragment())
        }
    }

    private fun getListener(): FragmentManager.OnBackStackChangedListener {

        return FragmentManager.OnBackStackChangedListener {
            val manager = supportFragmentManager
            if (manager != null) {
                val currFrag = manager.findFragmentById(dockFrameLayoutId) as? BaseFragment
                currFrag?.fragmentResume()
            }
        }
    }

    override fun getDockFrameLayoutId(): Int {
        return R.id.mainFrameLayout
    }

    override fun onMenuItemActionCalled(actionId: Int, data: String?) {
    }

    override fun setSubHeading(subHeadText: String?) {

    }
}
