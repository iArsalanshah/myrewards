package com.app.myrewards.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.app.myrewards.R


class SplashActivity : AppCompatActivity() {
    private val TIME_SPLASH = 2500 // in millis

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
    }

    public override fun onResume() {
        super.onResume()
        launchTimerAndTask()
    }

    private fun launchTimerAndTask() {
        Handler().postDelayed({ showMainActivity() }, TIME_SPLASH.toLong())
    }

    private fun showMainActivity() {
        val i = Intent(this, MainActivity::class.java)
        startActivity(i)
        finish()
    }
}