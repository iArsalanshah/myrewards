package com.app.myrewards.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.app.myrewards.R;

public class TitleBar extends RelativeLayout {

    private TextView txtTitle;
    private TextView btnRightCancel;
    private ImageView btnLeft;
    private ImageView btnRight;

    private OnClickListener menuButtonListener;
    private OnClickListener backButtonListener;
    private OnClickListener homeButtonListener;
    private OnClickListener cancelButtonListener;

    private Context context;


    public TitleBar(Context context) {
        super(context);
        this.context = context;
        initLayout(context);
    }

    public TitleBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        initLayout(context);
        if (attrs != null)
            initAttrs(context, attrs);
    }

    public TitleBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initLayout(context);
        if (attrs != null)
            initAttrs(context, attrs);
    }

    private void initAttrs(Context context, AttributeSet attrs) {
    }

    private void bindViews() {
        txtTitle = (TextView) this.findViewById(R.id.txt_subHead);
        btnRightCancel = (TextView) this.findViewById(R.id.btnRightCancel);
        btnRight = (ImageView) this.findViewById(R.id.btnRight);
        btnLeft = (ImageView) this.findViewById(R.id.btnLeft);
    }

    private void initLayout(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.header_main, this);
        bindViews();
    }

    public void hideButtons() {
        txtTitle.setVisibility(View.INVISIBLE);
        btnLeft.setVisibility(View.INVISIBLE);
        btnRight.setVisibility(View.INVISIBLE);
        btnRightCancel.setVisibility(View.INVISIBLE);
    }

    public void showBackButton() {
        btnLeft.setImageResource(R.drawable.ic_arrow_back_24dp);
        btnLeft.setVisibility(View.VISIBLE);
        btnLeft.setOnClickListener(backButtonListener);
    }

    public void setSubHeading(String heading) {
        txtTitle.setVisibility(View.VISIBLE);
        txtTitle.setText(heading);
    }

    public void showTitleBar() {
        this.setVisibility(View.VISIBLE);
    }

    public void hideTitleBar() {
        this.setVisibility(View.GONE);
    }

    public void setMenuButtonListener(OnClickListener listener) {
        menuButtonListener = listener;
    }

    public void setBackButtonListener(OnClickListener listener) {
        backButtonListener = listener;
    }

    public void setCancelButtonListener(OnClickListener listener) {
        cancelButtonListener = listener;
    }
}
