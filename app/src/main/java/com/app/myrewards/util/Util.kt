package com.app.myrewards.util

import android.content.res.Resources
import android.widget.EditText

val Int.dp: Int
    get() = (this / Resources.getSystem().displayMetrics.density).toInt()

val Int.px: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()

class Util {

    public fun validate(field: EditText): Boolean {
        return false
    }
}