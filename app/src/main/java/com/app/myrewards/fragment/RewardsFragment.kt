package com.app.myrewards.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.app.myrewards.R
import com.app.myrewards.adapter.RewardsAdapter
import com.app.myrewards.helper.GridSpacingItemDecoration
import com.app.myrewards.interfaces.OnViewHolderClick
import com.app.myrewards.util.dp
import kotlinx.android.synthetic.main.fragment_rewards.*


/**
 * A simple [Fragment] subclass.
 *
 */
class RewardsFragment : Fragment(), OnViewHolderClick {

    private lateinit var adapter: RewardsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_rewards, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
    }

    private fun initAdapter() {
        adapter = RewardsAdapter(activity, this)
        adapter.addAll(mutableListOf())
    }

    private fun initRecyclerView() {
        rewardsRecyclerView.layoutManager = GridLayoutManager(activity,2)
        rewardsRecyclerView.adapter = adapter

        val spanCount = 2 // 2 columns
        val spacing = 16.dp // 50px
        val includeEdge = true
        rewardsRecyclerView.addItemDecoration(GridSpacingItemDecoration(spanCount, spacing, includeEdge))

        var list = mutableListOf<Any>()
        list.add("")//Dummy Data
        list.add("")//Dummy Data
        list.add("")//Dummy Data
        list.add("")//Dummy Data
        list.add("")//Dummy Data
        adapter.addAll(list)
    }

    override fun onItemClick(view: View?, position: Int) {
        //TODO
    }
}
