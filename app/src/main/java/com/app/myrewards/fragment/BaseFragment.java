package com.app.myrewards.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import androidx.fragment.app.Fragment;
import com.app.myrewards.activity.DockActivity;
import com.app.myrewards.activity.MainActivity;
import com.app.myrewards.helper.BasePreferenceHelper;
import com.app.myrewards.views.TitleBar;


public abstract class BaseFragment extends Fragment {

    protected BasePreferenceHelper prefHelper;

//	protected  WebService webService;

    protected DockActivity myDockActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefHelper = new BasePreferenceHelper(getContext());

//		if (webService == null) {
//			webService = WebServiceFactory.getWebServiceInstanceWithCustomInterceptor(getDockActivity(), WebServiceConstants.BASE_URL);
//		}

        myDockActivity = getDockActivity();
    }

    protected void createClient() {
        // webService = WebServiceFactory.getInstanceWithBasicGsonConversion();
    }

    protected void loadingStarted() {
        isLoading = true;
    }

    protected void loadingFinished() {
        isLoading = false;
    }

    //it will gives us instance of DockActivity
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        myDockActivity = (DockActivity) context;
    }

    protected DockActivity getDockActivity() {
        return myDockActivity;
    }

    protected MainActivity getMainActivity() {
        return (MainActivity) getActivity();
    }

    protected TitleBar getTitleBar() {
        return getMainActivity().getTitleBar();
    }

    /**
     * This is called in the end to modify titlebar. after all changes.
     *
     * @param
     */
    public void setTitleBar(TitleBar titleBar) {
        titleBar.showTitleBar();
        // titleBar.refreshListener();
    }

    /**
     * Gets the preferred height for each item in the ListView, in pixels, after
     * accounting for screen density. ImageLoader uses this value to resize
     * thumbnail images to match the ListView item height.
     *
     * @return The preferred height in pixels, based on the current theme.
     */
    protected int getListPreferredItemHeight() {
        final TypedValue typedValue = new TypedValue();

        // Resolve list item preferred height theme attribute into typedValue
        getActivity().getTheme().resolveAttribute(
                android.R.attr.listPreferredItemHeight, typedValue, true);

        // Create a new DisplayMetrics object
        final DisplayMetrics metrics = new DisplayMetrics();

        // Populate the DisplayMetrics
        getActivity().getWindowManager().getDefaultDisplay()
                .getMetrics(metrics);

        // Return theme value based on DisplayMetrics
        return (int) typedValue.getDimension(metrics);
    }

//    protected void notImplemented() {
//		UIHelper.showLongToastInCenter( getActivity(), "Coming Soon" );
//    }

    private boolean isLoading;

    public void fragmentResume() {
        setTitleBar(((MainActivity) getDockActivity()).getTitleBar());
    }

    protected boolean checkLoading() {
        if (isLoading) {
//			UIHelper.showLongToastInCenter( getActivity(),
//					R.string.message_wait );
            return false;
        } else {
            return true;
        }
    }

}
