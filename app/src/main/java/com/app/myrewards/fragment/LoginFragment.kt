package com.app.myrewards.fragment


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.app.myrewards.activity.HomeActivity
import com.app.myrewards.api.ApiManager
import com.app.myrewards.model.LoginData
import com.app.myrewards.views.TitleBar
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_login.*
import org.jetbrains.anko.toast


/**
 * A simple [Fragment] subclass.
 *
 */
class LoginFragment : BaseFragment() {

    private val api by lazy {
        ApiManager.shared()
    }
    private var disposable: Disposable? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(com.app.myrewards.R.layout.fragment_login, container, false)
    }

    override fun setTitleBar(titleBar: TitleBar?) {
        super.setTitleBar(titleBar)
        titleBar?.hideTitleBar()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onClicks()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        disposable?.dispose()
    }

    private fun onClicks() {
        loginButton.setOnClickListener {
            if (validate()) {
                val intent = Intent(context, HomeActivity::class.java)
                dockActivity.finish()
                dockActivity.startActivity(intent)
                //loginAPI(emailEditText.text.toString(), passwordEditText.text.toString())
            }
        }
        signUpTextView.setOnClickListener {
            dockActivity.replaceDockableFragment(SignUpFragment())
        }
    }

    private fun validate(): Boolean {
        if (emailEditText.text.toString().isEmpty()) {
            emailEditText.error = "Email is empty"
            return false
        }
        emailEditText.error = null
        if (passwordEditText.text.toString().isEmpty()) {
            passwordEditText.error = "Password is empty"
            return false
        }
        passwordEditText.error = null
        return true
    }

    private fun loginAPI(userName: String, pwd: String) {
        disposable = api.login("login", userName, pwd)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    onSuccessLogin(it.body)
                },
                {
                    activity?.toast(it.localizedMessage)
                }
            )
    }

    private fun onSuccessLogin(data: LoginData?) {
        if (data == null) {
            activity?.toast("Something went wrong...")
        } else {
            prefHelper.setLoginStatus(true)
            prefHelper.userData = data
            val intent = Intent(context, HomeActivity::class.java)
            dockActivity.finish()
            dockActivity.startActivity(intent)
        }
    }
}
