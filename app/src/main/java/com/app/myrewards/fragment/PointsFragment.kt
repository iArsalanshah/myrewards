package com.app.myrewards.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.myrewards.R
import com.app.myrewards.adapter.ActivityPointsAdapter
import com.app.myrewards.interfaces.OnViewHolderClick
import kotlinx.android.synthetic.main.fragment_points_earned.*

/**
 * A simple [Fragment] subclass.
 *
 */
class PointsFragment : Fragment(), OnViewHolderClick {

    private lateinit var adapter: ActivityPointsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_points_earned, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
    }

    private fun initAdapter() {
        adapter = ActivityPointsAdapter(activity, this)
        adapter.addAll(mutableListOf())
    }

    private fun initRecyclerView() {
        pointsRecyclerView.layoutManager = LinearLayoutManager(activity)
        pointsRecyclerView.adapter = adapter

        var list = mutableListOf<Any>()
        list.add("")//Dummy Data
        adapter.addAll(list)
    }

    override fun onItemClick(view: View?, position: Int) {
        //TODO
    }
}