package com.app.myrewards.fragment


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import com.app.myrewards.R
import com.app.myrewards.activity.HomeActivity
import com.app.myrewards.views.TitleBar
import kotlinx.android.synthetic.main.fragment_sign_up.*

/**
 * A simple [Fragment] subclass.
 *
 */
class SignUpFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_up, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onClicks()
    }

    override fun setTitleBar(titleBar: TitleBar?) {
        super.setTitleBar(titleBar)
        titleBar?.hideTitleBar()
    }

    private fun onClicks() {
        signUpButton.setOnClickListener {
            if (validate()) {
                val intent = Intent(context, HomeActivity::class.java)
                dockActivity.finish()
                dockActivity.startActivity(intent)
            }
        }
    }

    private fun validate(): Boolean {
        if (checkEditText(fullNameEditText)) {
            return false
        }
        fullNameEditText.error = null

        if (checkEditText(email)) {
            return false
        }
        email.error = null

        if (checkEditText(countryEditText)) {
            return false
        }
        countryEditText.error = null

        if (checkEditText(cityEditText)) {
            return false
        }
        cityEditText.error = null

        if (checkEditText(password)) {
            return false
        }
        password.error = null
        return false
    }

    private fun checkEditText(editText: EditText): Boolean {
        if (editText.text.toString().isEmpty()) {
            editText.error = "This field is mandatory"
            return true
        }
        return false
    }
}
