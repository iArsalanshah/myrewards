package com.app.myrewards.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.app.myrewards.R;
import com.app.myrewards.interfaces.OnViewHolderClick;


/**
 * Created by syed.shah on 3/16/18.
 */

public class RewardsAdapter extends RecyclerViewListAdapter<Object> {
    private LayoutInflater inflater;

    public RewardsAdapter(Context context, OnViewHolderClick onViewHolderClick) {
        super(context, onViewHolderClick);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    protected View createView(Context context, ViewGroup viewGroup, int viewType) {
        return inflater.inflate(R.layout.item_rewards, viewGroup, false);
    }

    @Override
    protected void bindView(Object item, RecyclerviewViewHolder viewHolder) {
        if (item != null) {
            ImageView gridItemImage = (ImageView) viewHolder.getView(R.id.gridItemImage);
            TextView gridItemTitle = (TextView) viewHolder.getView(R.id.gridItemTitle);
            TextView gridItemPrice = (TextView) viewHolder.getView(R.id.gridItemPrice);
        }
    }

    @Override
    protected int bindItemViewType(int position) {
        return 0;
    }

    @Override
    protected int bindItemId(int position) {
        return 0;
    }
}
