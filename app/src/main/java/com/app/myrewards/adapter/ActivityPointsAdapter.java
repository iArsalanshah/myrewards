package com.app.myrewards.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.app.myrewards.R;
import com.app.myrewards.interfaces.OnViewHolderClick;

/**
 * Created by syed.shah on 3/16/18.
 */

public class ActivityPointsAdapter extends RecyclerViewListAdapter<Object> {
    private LayoutInflater inflater;

    public ActivityPointsAdapter(Context context, OnViewHolderClick onViewHolderClick) {
        super(context, onViewHolderClick);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    protected View createView(Context context, ViewGroup viewGroup, int viewType) {
        return inflater.inflate(R.layout.item_activity_points, viewGroup, false);
    }

    @Override
    protected void bindView(Object item, RecyclerviewViewHolder viewHolder) {
        if (item != null) {
            ImageView img = (ImageView) viewHolder.getView(R.id.img);
            TextView activityPointTitleTv = (TextView) viewHolder.getView(R.id.activityPointTitleTv);
            TextView activityPointsTv = (TextView) viewHolder.getView(R.id.activityPointsTv);
        }
    }

    @Override
    protected int bindItemViewType(int position) {
        return 0;
    }

    @Override
    protected int bindItemId(int position) {
        return 0;
    }
}
