package com.app.myrewards.api

import com.app.myrewards.constant.BASE_URL
import com.app.myrewards.model.BaseResponse
import com.app.myrewards.model.LoginData
import com.app.myrewards.model.NoResponse
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ApiManager {

    /*LOGIN API*/
    @GET("/api/mobileservice.php")//?action=login
    fun login(
        @Query("action") action: String,
        @Query("username") username: String,
        @Query("password") password: String
    ): Observable<BaseResponse<LoginData>>

    /*REGISTER API*/
    @FormUrlEncoded
    @POST("api/mobileservice.php")//?action=register
    fun register(
        @Query("action") action: String,
        @Field("username") name: String,
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("user_type") user_type: String
    ): Observable<BaseResponse<NoResponse>>


    /*SCAN API*/
    @GET("api/mobileservice.php")//?action=qrcodeScan
    fun scanAPI(
        @Query("action") action: String,
        @Query("user_id") user_id: String,
        @Query("qr_code") qr_code: String
    ): Observable<BaseResponse<NoResponse>>


    companion object {
        fun shared(): ApiManager {

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(
                    RxJava2CallAdapterFactory.create()
                )
                .addConverterFactory(
                    GsonConverterFactory.create()
                )
                .baseUrl(BASE_URL)
                .build()

            return retrofit.create(ApiManager::class.java)
        }
    }
}