package com.app.myrewards.helper

enum class MarkerSelectionMode {
    DEFAULT,
    DIRECTION_STEP_1, DIRECTION_STEP_2,
    RADIUS
}
