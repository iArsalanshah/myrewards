package com.app.myrewards.helper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import com.app.myrewards.model.LoginData;
import com.google.gson.Gson;


public class BasePreferenceHelper extends PreferenceHelper {

    private Context context;

    private static final String KEY_LOGIN_STATUS = "islogin";

    private static final String FILENAME = "preferences";

    private static final String KEY_USER_DATA = "key_user";


    public BasePreferenceHelper(Context c) {
        this.context = c;
    }

    public SharedPreferences getSharedPreferences() {
        return context.getSharedPreferences(FILENAME, Activity.MODE_PRIVATE);
    }

    public void setLoginStatus(boolean isLogin) {
        putBooleanPreference(context, FILENAME, KEY_LOGIN_STATUS, isLogin);
    }

    public boolean isLogin() {
        return getBooleanPreference(context, FILENAME, KEY_LOGIN_STATUS);
    }

    public LoginData getUserData() {
        return new Gson().fromJson(
                getStringPreference(context, FILENAME, KEY_USER_DATA), LoginData.class);
    }

    public void setUserData(LoginData data) {
        putStringPreference(context, FILENAME, KEY_USER_DATA, new Gson().toJson(data));
    }

}
