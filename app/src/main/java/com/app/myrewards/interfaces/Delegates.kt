package com.app.myrewards.interfaces

interface APIDelegate {

    fun onSuccess()

    fun onFailure()
}