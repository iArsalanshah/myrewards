package com.app.myrewards.model

import com.google.gson.annotations.SerializedName


data class BaseResponse<T>(
    @SerializedName("body")
    val body: T? = null,
    @SerializedName("header")
    val header: Header = Header()
) {
    fun isSuccess(): Boolean {
        return header.success.isNotEmpty() && header.success == "1"
    }

    fun message(): String {
        return header.message
    }
}

class NoResponse()

data class LoginData(
    @SerializedName("users")
    val users: Users = Users()
)

data class Users(
    @SerializedName("comments")
    val comments: String = "",
    @SerializedName("created_at")
    val createdAt: String = "",
    @SerializedName("email")
    val email: String = "",
    @SerializedName("id")
    val id: String = "",
    @SerializedName("password")
    val password: String = "",
    @SerializedName("status")
    val status: String = "",
    @SerializedName("user_type")
    val userType: String = "",
    @SerializedName("username")
    val username: String = ""
)

data class Header(
    @SerializedName("message")
    val message: String = "",
    @SerializedName("success")
    val success: String = ""
)